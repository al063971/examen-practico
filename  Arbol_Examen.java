package exa_ordinario;

/**
 *
 * @author eduardo
 */

public class Arbol_Examen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int dato;
        String nombre;
        ArbolBinario_Examen miArbol = new ArbolBinario_Examen();
        miArbol.agregarNodo(69, "E");
        miArbol.agregarNodo(68, "D");
        miArbol.agregarNodo(85, "U");
        miArbol.agregarNodo(65, "A");
        miArbol.agregarNodo(82, "R");
        miArbol.agregarNodo(79, "O");
        miArbol.agregarNodo(73, "I");
        miArbol.agregarNodo(86, "V");
        miArbol.agregarNodo(78, "N");
        miArbol.agregarNodo(84, "T");
        miArbol.agregarNodo(72, "H");
        miArbol.agregarNodo(90, "Z");
        
        System.out.println("inOrden");
        System.out.println("DUARTE HERNANDEZ EDUARDO IVAN, 63971 ");
        if (!miArbol.estaVacio()) {
            miArbol.inOrden(miArbol.raiz);
        }
    }
}
